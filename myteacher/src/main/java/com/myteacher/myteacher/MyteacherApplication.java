package com.myteacher.myteacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyteacherApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyteacherApplication.class, args);
	}

}
